#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <exception>
#include <string>

#include "perceptron_solver.h"

using namespace std;

PerceptronSolver::PerceptronSolver(vector<double> init_weights){
    this->weights = init_weights;
}

vector<vector<double> > PerceptronSolver::read_csv(string filename){
    vector<vector<double> > content;
    vector<double> row;
    string line, word;
    fstream file(filename, ios::in);
    if (file.is_open())
    {
        while (getline(file, line))
        {
            row.clear();
            stringstream str(line);

            while (getline(str, word, ';'))
                row.push_back(stod(word));
            content.push_back(row);
        }
    }
    else
    {
        throw std::runtime_error("cannot open file " + filename);
        return content;
    }
    return content;
}

int PerceptronSolver::dot_product(vector<double> a, vector<double> b)
{
    if (a.size() != b.size())
    {
        throw std::runtime_error("vectors must have equal size");
    }
    int ret = 0;
    for (int i = 0; i < a.size(); i++)
    {
        ret += a.at(i) * b.at(i);
    }
    return ret;
}

vector<double> PerceptronSolver::vector_calc(vector<double> main_vector, vector<double> operand, char action)
{
    vector<double> ret;
    for (int i = 0; i < main_vector.size(); i++)
    {
        double current_it_operand = operand.size() == 1 ? operand[0] : operand[i];
        switch (action)
        {
        case '-':
            ret.push_back(main_vector[i] - current_it_operand);
            break;
        case '+':
            ret.push_back(main_vector[i] + current_it_operand);
            break;
        case '*':
            ret.push_back(main_vector[i] * current_it_operand);
            break;
        case '/':
            ret.push_back(main_vector[i] / current_it_operand);
            break;
        default:
            throw std::runtime_error("unexpected action provided to the vector calculator");
        }
    }
    return ret;
}

vector<double> PerceptronSolver::fit(vector<vector<double> > input, vector<int> targets, int max_iter, double learning_rate)
{
    vector<double> temp_weights;
    std::copy(this->weights.begin(), this->weights.end(), std::back_inserter(temp_weights));
    if (input[0].size() != temp_weights.size() - 1)
    {
        throw std::runtime_error("weights array length must be equal to input array 1 dim length");
        return vector<double>();
    }
    int i = 0;
    int total_iterations_needed = 0;
    int min_misclassification_count = INT_MAX;
    while (i <= max_iter)
    {
        int misclassifications_count = 0;
        int j = 0;
        for (vector<double> record : input)
        {
            // ON EACH ITER pushing 1 as a bias value to the vector's back.
            record.push_back(1);
            int raw_prediction = dot_product(record, temp_weights);
            int prediction = raw_prediction >= 0 ? 1 : 0;
            int delta_error = targets[j] - prediction;
            vector<double> op_vector;
            op_vector.push_back(delta_error * learning_rate);
            vector<double> biased_delta = vector_calc(record, op_vector, '*');
            temp_weights = vector_calc(temp_weights, biased_delta, '+');
            j++;
            // adding stats
            if (delta_error != 0)
            {
                misclassifications_count++;
            }
            record.pop_back();
        }
        if (misclassifications_count < min_misclassification_count){
            min_misclassification_count = misclassifications_count;
        }
        if (misclassifications_count == 0)
        {
            total_iterations_needed = i;
            break;
        }
        misclassifications_count = 0;
        i++;
    }
    if (total_iterations_needed == 0)
    {
        cout << "reached the upper bound of max cykle iterations: " << max_iter << endl;
    }
    else
    {
        cout << "total iterations needed to converge: " << total_iterations_needed << endl;
    }
     cout << "min total items classified incorrectly: " << min_misclassification_count << endl;
    this->weights = temp_weights;
    return this->weights;
}

vector<int> PerceptronSolver::predict(vector<vector<double> > in){
    if (in[0].size() != this->weights.size()-1){
        throw std::runtime_error("expected size of array: " + std::to_string(this->weights.size()-1) + "; got: " + std::to_string(in.size()-1));
        return vector<int>();
    }
    vector<int> ret;
    for (int i = 0; i < in.size(); i++){
        in[i].push_back(1);
        double raw_y = this->dot_product(in[i], this->weights);
        int prediction_y = raw_y >= 0 ? 1 : 0;
        ret.push_back(prediction_y);
    }
    return ret;
}