#define IRIS_DATASET_LOC "/Users/ivansmaliakou/mag-exam-prep/perceptron/data/Iris.csv"
#include <vector>

using namespace std;

class PerceptronSolver {
    private:
        // properties
        vector<double> weights;

        // methods
        vector<double> vector_calc(vector<double> main_vector, vector<double> operand, char action);
        int dot_product(vector<double> a, vector<double> b);

    public:
        PerceptronSolver(vector<double> init_weigths);
        ~PerceptronSolver();
        vector<double> fit(vector<vector<double> > input, vector<int> targets, int max_iter, double learning_rate);
        vector<int> predict(vector<vector<double> > in);
        vector<vector<double> > read_csv(string filename);
};