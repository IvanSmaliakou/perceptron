#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <exception>
#include <string>
#include "perceptron_solver.cpp"

int main()
{
    vector<double> init_weights;
    init_weights.push_back(0);
    init_weights.push_back(0);
    init_weights.push_back(0);
    PerceptronSolver *solver = new PerceptronSolver(init_weights);
    vector<vector<double> > content = solver->read_csv(IRIS_DATASET_LOC);
    vector<int> targets;
    vector<vector<double> > input;
    for (int i = 0; i < content.size(); i++)
    {
        targets.push_back(int(content[i][content[i].size() - 1]));
        input.push_back(vector<double>());
        for (int j = 0; j < content[i].size()-1; j++)
        {
            input[i].push_back(content[i][j]);
        }
    }
    vector<double> output_weights = solver->fit(input, targets, 10000, 0.01);
    for (int i = 0; i < output_weights.size(); i++)
    {
        cout << "param " << i << ": " << output_weights[i] << endl;
    }

    // testing
    vector<double> must_be_classified0; must_be_classified0.push_back(50); must_be_classified0.push_back(30);
    vector<double> must_be_classified1; must_be_classified1.push_back(-100); must_be_classified1.push_back(-100);
    vector<vector<double> > test_dataset;
    test_dataset.push_back(must_be_classified0); test_dataset.push_back(must_be_classified1);
    vector<int> predictions = solver->predict(test_dataset);
    
    
    cout << "must be classified as 0: " << predictions[0] << endl;
    cout << "must be classified as 1: " << predictions[1] << endl;
    return 0;
}